﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SimpleFileBrowser;

public class ImportManager : MonoBehaviour
{
    public void loadMeshButtonHandel()
    {
        ShowFileLoadBrowser();
    }
    void ShowFileLoadBrowser()
    {
        FileBrowser.SetFilters(true, new FileBrowser.Filter("Mesh file", ".dae" , ".fbx" , ".obj" , ".blend" , ".abc"));
        FileBrowser.SetDefaultFilter(".dae");
        FileBrowser.SetExcludedExtensions(".lnk", ".tmp", ".zip", ".rar", ".exe");
        FileBrowser.AddQuickLink("CurrentProject", @"C:\Projects\Unity\Personal", null);
        StartCoroutine(ShowLoadDialogCoroutine());
    }

    IEnumerator ShowLoadDialogCoroutine()
    {
        yield return FileBrowser.WaitForLoadDialog(false, @"C:\Projects\Unity\Personal", "Load Mesh", "Load");
        if(FileBrowser.Success)
            LoadMesh(FileBrowser.Result);
        Debug.Log(FileBrowser.Success + " " + FileBrowser.Result);
    }

    void LoadMesh(System.String fileName)
    {
        AlembicImportManager.instance.ImportAlembicFile(fileName);
    }
}
