﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UTJ.Alembic;
public class AlembicImportManager : MonoBehaviour
{
    public static AlembicImportManager instance = null;

    [SerializeField] public AlembicStreamSettings streamSettings = new AlembicStreamSettings();

    aiConfig m_config;
    aiContext m_context;
    bool m_loaded;
    public aiConfig config { get { return m_config; } }
    AlembicStream abcStream;
    AlembicStream.ImportContext m_importContext;

    private void Awake()
    {
        instance = this;
    }

    public bool ImportAlembicFile(string destPath)
    {
        if (Path.GetExtension(destPath.ToLower()) == ".abc")
           return load(destPath);
        else
            Debug.LogError("Alembic mesh extention not found");
        return false;
    }
    bool load(string filePath)
    {
        var fileName = Path.GetFileNameWithoutExtension(filePath);
        var go = new GameObject(fileName);
        var streamDescriptor = ScriptableObject.CreateInstance<AlembicStreamDescriptor>();
        streamDescriptor.name = go.name + "_ABCDesc";
        streamDescriptor.pathToAbc = filePath;
        streamDescriptor.settings = streamSettings;

        abcStream = new AlembicStream(go, streamDescriptor);
  
        m_context = aiContext.Create(abcStream.abcTreeRoot.gameObject.GetInstanceID());

        m_config.swapHandedness = streamSettings.swapHandedness;
        m_config.flipFaces = streamSettings.flipFaces;
        m_config.aspectRatio = Screen.width/Screen.height;
        m_config.scaleFactor = streamSettings.scaleFactor;
        m_config.normalsMode = streamSettings.normals;
        m_config.tangentsMode = streamSettings.tangents;
        m_config.turnQuadEdges = streamSettings.turnQuadEdges;
        m_config.interpolateSamples = streamSettings.interpolateSamples;
        m_config.importPointPolygon = streamSettings.importPointPolygon;
        m_config.importLinePolygon = streamSettings.importLinePolygon;
        m_config.importTrianglePolygon = streamSettings.importTrianglePolygon;

        m_context.SetConfig(ref m_config);
        m_loaded = m_context.Load(filePath);

        if (m_loaded)
        {
            var top = m_context.topObject;
            Debug.Log("success to load alembic file = " + top.childCount );
            UpdateAbcTree(m_context, abcStream.abcTreeRoot, 0.0f, true);
            CollectSubAssets(abcStream.abcTreeRoot);
        }
        else
        {
            Debug.LogError("failed to load alembic");
        }
        return (m_loaded);
    }
    
    void UpdateAbcTree(aiContext ctx, AlembicTreeNode node, double time, bool createMissingNodes)
    {
        var top = ctx.topObject;
        if (!top)
            return;

        m_importContext = new AlembicStream.ImportContext
        {
            alembicTreeNode = node,
            createMissingNodes = createMissingNodes,
        };
        top.EachChild(ImportCallback);
        m_importContext = null;
    }

    void ImportCallback(aiObject obj)
    {
        var ic = m_importContext;
        AlembicTreeNode treeNode = ic.alembicTreeNode;
        AlembicTreeNode childTreeNode = null;

        aiSchema schema = obj.AsXform();
        if (!schema) schema = obj.AsPolyMesh();
        if (!schema) schema = obj.AsCamera();
        if (!schema) schema = obj.AsPoints();

        if (schema)
        {
           
            // Get child. create if needed and allowed.
            string childName = obj.name;

            // Find targetted child GameObj
            GameObject childGO = null;

            var childTransf = treeNode.gameObject == null ? null : treeNode.gameObject.transform.Find(childName);
            if (childTransf == null)
            {
                if (!ic.createMissingNodes)
                {
                    obj.enabled = false;
                    return;
                }
                else
                {
                    obj.enabled = true;
                }

                childGO = new GameObject { name = childName };
                childGO.GetComponent<Transform>().SetParent(treeNode.gameObject.transform, false);
            }
            else
                childGO = childTransf.gameObject;

            childTreeNode = new AlembicTreeNode() { stream = abcStream, gameObject = childGO };
            treeNode.children.Add(childTreeNode);

            // Update
            AlembicElement elem = null;

            if (obj.AsXform() && streamSettings.importXform)
                elem = childTreeNode.GetOrAddAlembicObj<AlembicXform>();
            else if (obj.AsCamera() && streamSettings.importCameras)
                elem = childTreeNode.GetOrAddAlembicObj<AlembicCamera>();
            else if (obj.AsPolyMesh() && streamSettings.importMeshes)
                elem = childTreeNode.GetOrAddAlembicObj<AlembicMesh>();
            else if (obj.AsPoints() && streamSettings.importPoints)
                elem = childTreeNode.GetOrAddAlembicObj<AlembicPoints>();

            if (elem != null)
            {
                elem.AbcSetup(obj, schema);
                elem.AbcPrepareSample();
                schema.UpdateSample(ref ic.ss);
                elem.AbcSyncDataBegin();
                elem.AbcSyncDataEnd();
            }
        }
        else
        {
            obj.enabled = false;
        }

        ic.alembicTreeNode = childTreeNode;
        obj.EachChild(ImportCallback);
        ic.alembicTreeNode = treeNode;
    }
    class Subassets
    {
        Material m_defaultMaterial;
        Material m_defaultPointsMaterial;
        Material m_defaultPointsMotionVectorMaterial;

        public Subassets()
        {
        }

        public Material defaultMaterial
        {
            get
            {
                if (m_defaultMaterial == null)
                {
                    m_defaultMaterial = new Material(Shader.Find("Alembic/Standard"));
                    m_defaultMaterial.hideFlags = HideFlags.NotEditable;
                    m_defaultMaterial.name = "Default Material";
                }
                return m_defaultMaterial;
            }
        }

        public Material pointsMaterial
        {
            get
            {
                if (m_defaultPointsMaterial == null)
                {
                    m_defaultPointsMaterial = new Material(Shader.Find("Alembic/Points Standard"));
                    m_defaultPointsMaterial.hideFlags = HideFlags.NotEditable;
                    m_defaultPointsMaterial.name = "Default Points";
                }
                return m_defaultPointsMaterial;
            }
        }

        public Material pointsMotionVectorMaterial
        {
            get
            {
                if (m_defaultPointsMotionVectorMaterial == null)
                {
                    m_defaultPointsMotionVectorMaterial = new Material(Shader.Find("Alembic/PointsMotionVectors"));
                    m_defaultPointsMotionVectorMaterial.hideFlags = HideFlags.NotEditable;
                    m_defaultPointsMotionVectorMaterial.name = "Points Motion Vector";
                }
                return m_defaultPointsMotionVectorMaterial;
            }
        }
    }
    void CollectSubAssets(AlembicTreeNode node)
    {
        Subassets subassets = new Subassets();

        int submeshCount = 0;
        var meshFilter = node.gameObject.GetComponent<MeshFilter>();
        if (meshFilter != null)
        {
            var m = meshFilter.sharedMesh;
            submeshCount = m.subMeshCount;
            m.name = node.gameObject.name;
        }

        var renderer = node.gameObject.GetComponent<MeshRenderer>();
        if (renderer != null)
        {
            var mats = new Material[submeshCount];
            for (int i = 0; i < submeshCount; ++i)
                mats[i] = subassets.defaultMaterial;
            renderer.sharedMaterials = mats;
        }

        var apr = node.gameObject.GetComponent<AlembicPointsRenderer>();
        if (apr != null)
        {
            var cubeGO = GameObject.CreatePrimitive(PrimitiveType.Cube);
            apr.sharedMesh = cubeGO.GetComponent<MeshFilter>().sharedMesh;
            DestroyImmediate(cubeGO);

            apr.sharedMaterials = new Material[] { subassets.pointsMaterial };
            apr.motionVectorMaterial = subassets.pointsMotionVectorMaterial;
        }

        foreach (var child in node.children)
            CollectSubAssets(child);
    }
}
